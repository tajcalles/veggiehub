<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veggie extends Model
{
    protected $fillable = [
        'veggie_name',
        'veggie_city',
        'veggie_price'
    ];
}
