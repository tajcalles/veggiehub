<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Veggie;

class VeggieController extends Controller {

    public function theBushel()
    {
        // Variable created to bring in the 'Veggie' model, and all information in that table.
        $veggies = Veggie::all();

        return view ('veggies.theBushel', compact('veggies'));

    }

    public function grow()
    {
        // When called, shows the client the form to 'create a veggie'
        return view('veggies.grow');

    }

    public function harvest(Request $request)
    {
        // Ensuring that the request contains valid datatypes, according to the paramaters set in the database
        $request->validate([
            'veggie_name' => 'required',
            'veggie_city' => 'required',
            'veggie_price' => 'required|integer'
        ]);
        // Variable created that fills the required fields with the information entered in, client-side.
        $veggie = new Veggie([
            'veggie_name' => $request->get('veggie_name'),
            'veggie_city' => $request->get('veggie_city'),
            'veggie_price' => $request->get('veggie_price')
        ]);
        $veggie->save();
        return redirect('/theBushel')->with('success', 'Veggie has been added!');

    }

    public function pick($id)
    {
        // Variable created to call in the model, and find a specific "veggie" by its ID in the table
        $veggie = Veggie::find($id);
        // Returns the "show" page for the veggie with the specific ID above, and passes the "veggie" variable to the view to display its various properties.
        return view('veggies.pick', compact('veggie'));
    }

    public function prune($id)
    {
        // Variable created to call in the model, and find a specific "veggie" by its ID in the table
        $veggie = Veggie::find($id);
        // Returns the "edit" view, and passes the veggie
        return view('veggies.prune', compact('veggie'));
    }

    public function graft(Request $request, $id)
    {
        // The incoming data from the "prune" needs to satisfy all required fields specified by the parameters below.
        $request->validate([
            'veggie_name' => 'required',
            'veggie_city' => 'required',
            'veggie_price' => 'required|integer'
        ]);
        // The prune "form" is labeled so that the data entered client side replaces the correct fields in the database.
        $veggie = Veggie::find($id);
        $veggie->veggie_name = $request->get('veggie_name');
        $veggie->veggie_city = $request->get('veggie_city');
        $veggie->veggie_price = $request->get('veggie_price');
        $veggie->save();

        return redirect('/theBushel')->with('success', 'Veggie has been updated!');
    }

    public function torch($id)
    {
        // Deletes the veggie with the corresponding ID. 
        $veggie = Veggie::find($id);
        $veggie->delete();

        return redirect('/theBushel')->with('success', 'Veggie listing has been deleted.');

    }
}
