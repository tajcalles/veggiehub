<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Veggie;

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/grow', 'VeggieController@grow');
Route::post('harvest', 'VeggieController@harvest');
Route::get('/theBushel', 'VeggieController@theBushel');

Route::get('/pick/{id}', 'VeggieController@pick'); // Show page for specific veggie (by id)
Route::get('/prune/{id}', 'VeggieController@prune'); // Show page for editing properties of specific veggie (by id)
Route::patch('/graft/{id}', 'VeggieController@graft'); // Saves changes to edited "veggie" to the database
Route::delete('/torch/{id}', 'VeggieController@torch'); // Deletes resource with the corresponding ID
Auth::routes();

Route::get('/home', 'VeggieController@theBushel')->name('home');
