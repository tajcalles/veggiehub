@extends('layouts.master')

@section('content')

<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card-uper">
    <div class="card-header">
        Edit Veggie Listing
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
    <form method="post" action="/graft/{{$veggie->id}}">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="name">Veggie Name: </label>
            <input type="text" class="form-control" name="veggie_name" value={{ $veggie->veggie_name }} />
        </div>
        <div class="form-group">
            <label for="city">Veggie City: </label>
            <input type="text" class="form-control" name="veggie_city" value={{ $veggie->veggie_city }} />
        </div>
        <div class="form-group">
            <label for="price">Veggie Price: </label>
            <input type="text" class="form-control" name="veggie_price" value={{ $veggie->veggie_price }} />
        </div>
        <button type="submit" class="btn btn-primary">Submit Changes</button>
    </form>
    </div>
</div>

@endsection
