@extends('layouts.master')

@section('content')

<div class="card uper">
    <div class="card-header">
        Add Listing
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
    <form method="post" action="/harvest">
        <div class="form-group">
            @csrf
            <label for="name">Veggie Name: </label>
            <input type="text" class="form-control" name="veggie_name" />
        </div>
        <div class="form-group">
            <label for="city">Veggie City: </label>
            <input type="text" class="form-control" name="veggie_city" />
        </div>
        <div class="form-group">
            <label for="name">Veggie Price: </label>
            <input type="text" class="form-control" name="veggie_price" />
        </div>
        <button type="submit" class="btn btn-primary">Create Listing</button>
    </form>
    </div>
</div>

@endsection
