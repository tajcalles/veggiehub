@extends('layouts.master')

@section('content')

<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img class="card-img-top" src="/veggiebasket.jpeg" alt="Card image cap">
                    <div class="card-body">
                        <span class="card-text">{{$veggie->veggie_name}}</span>
                        <p class="card-text">Grown in: {{$veggie->veggie_city}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a href="/prune/{{$veggie->id}}" class="btn btn-primary">Edit Listing</a>
                                <form action="/torch/{{$veggie->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </div>
                            <small class="text-muted">${{$veggie->veggie_price}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
