@extends('layouts.master')

@section('content')

@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif

<main role="main">

  <section class="jumbotron text-center centerpiece">
    <div class="container">
      <h1 class="jumbotron-heading">CRUDdy Marketplace</h1>
      <p class="lead text-muted">This mock marketplace is to help the young, budding developer Taj K. Calles further improve his skills with CRUD actions, in all shapes and forms. Please create a listing and sell your vegetables here! Click below and help me grow! </p>
      <p>
        <a href="/grow" class="btn btn-primary my-2">Create a Listing</a>
        <a href="/index" class="btn btn-secondary my-2">View All Listings</a>
      </p>
    </div>
  </section>


<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            @foreach($veggies as $veggie)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img class="card-img-top" src="/veggiebasket.jpeg" alt="Card image cap">
                    <div class="card-body">
                        <span class="card-text">{{$veggie->veggie_name}}</span>
                        <p class="card-text">Grown in: {{$veggie->veggie_city}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-outline-secondary" href="/pick/{{$veggie->id}}">Edit Listing</a>
                              <a class="btn btn-sm btn-outline-secondary" href="/add">Add to Favs</a>
                            </div>
                            <small class="text-muted">${{$veggie->veggie_price}}</small>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


</main>
@endsection
